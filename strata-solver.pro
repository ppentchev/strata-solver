#!/usr/bin/yap -L
%
% A naive implementation of a Strata puzzle solver.
% Please see the README file for more information.
%
% Copyright (C) 2015  Peter Pentchev <roam@ringlet.net>
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions
% are met:
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
% OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
% HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
% LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
% OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
% SUCH DAMAGE.

% Make append/3 and reverse/1 available.

:- use_module(library(lists)).

%%%%%%%
%
% The top-level solver implementation: +BOARD, -SOLUTION

strata_solver(B, S) :- sts_get_size(N, B), sts_build_sets(N, L),
	sts_explode(B, BX), !, sts_solver(BX, L, 1, RS), reverse(RS, S).

sts_get_size(N, [H | T]) :- length(H, N), length(T, N1), N =:= N1 + 1, !,
	sts_check_rows_len(T, N).

sts_check_rows_len([], _).
sts_check_rows_len([H | T], N) :- length(H, N), sts_check_rows_len(T, N).

sts_def_digit(["1", "2", "3", "4", "5", "6", "7", "8"]).
sts_def_alpha(["A", "B", "C", "D", "E", "F", "G", "H"]).

sts_build_sets(N, S) :- sts_build_rows(N, R), sts_build_columns(N, C),
	append(R, C, S).

sts_build_rows(N, R) :- sts_def_digit(A), sts_build_rows_loop(1, N, R, A).

sts_build_rows_loop(N, N, [[L, R]], [L | _]) :- !, sts_build_row(1, N, N, R).
sts_build_rows_loop(Y, N, [[L, R] | TR], [L | A]) :- sts_build_row(1, Y, N, R),
	Y1 is Y + 1, !, sts_build_rows_loop(Y1, N, TR, A).

sts_build_row(N, Y, N, [[N, Y]]) :- !.
sts_build_row(X, Y, N, [[X, Y] | T]) :- X1 is X + 1, !,
	sts_build_row(X1, Y, N, T).

sts_build_columns(N, C) :- sts_def_alpha(A),
	sts_build_columns_loop(1, N, C, A).

sts_build_columns_loop(N, N, [[L, R]], [L | _]) :- !,
	sts_build_column(N, 1, N, R).
sts_build_columns_loop(X, N, [[L, R] | TR], [L | A]) :-
	sts_build_column(X, 1, N, R), X1 is X + 1, !,
	sts_build_columns_loop(X1, N, TR, A).

sts_build_column(X, N, N, [[X, N]]) :- !.
sts_build_column(X, Y, N, [[X, Y] | T]) :- Y1 is Y + 1, !,
	sts_build_column(X, Y1, N, T).

sts_explode(B, BX) :- sts_explode_loop(1, B, BX).

sts_explode_loop(_, [], []).
sts_explode_loop(Y, [R | TB], X) :- sts_explode_row(1, Y, R, RX), Y1 is Y + 1,
	!, sts_explode_loop(Y1, TB, TX), !, append(RX, TX, X).

sts_explode_row(_, _, [], []).
sts_explode_row(X, Y, [0 | T], TX) :- !, X1 is X + 1,
	sts_explode_row(X1, Y, T, TX).
sts_explode_row(X, Y, [H | T], [[X, Y, H] | TX]) :- X1 is X + 1,
	sts_explode_row(X1, Y, T, TX).

sts_solver([], [], _, []) :- !.
sts_solver([], _, 1, _) :- !, fail.
sts_solver([], L, 0, S) :- !, fill_ones(L, S).

sts_solver(B, L, _, [[ID, CLR] | TS]) :- sts_match_one(B, L, ID, CLR, NB, NL),
	sts_solver(NB, NL, 0, TS).

fill_ones([], []) :- !.
fill_ones([[ID | _] | T], [[ID, 1] | TS]) :- fill_ones(T, TS).

sts_match_one(_, [], _, _, _, _) :- !, fail.
sts_match_one(B, [[ID, R] | TL], ID, CLR, NB, TL) :- sts_match(B, R, 0, CLR, NB).
sts_match_one(B, [L | TL], ID, CLR, NB, [L | TNL]) :-
	sts_match_one(B, TL, ID, CLR, NB, TNL).

sts_match(_, [], 0, _, _) :- !, fail.
sts_match(B, [], CLR, CLR, B) :- !.
sts_match([], _, 0, _, _) :- !, fail.
sts_match([], _, CLR, CLR, []) :- !.

sts_match([[X, Y, BC] | TB], [[X, Y] | TR], 0, BC, NB) :- !,
	sts_match(TB, TR, BC, BC, NB).
sts_match([[X, Y, BC] | TB], [[X, Y] | TR], BC, BC, NB) :- !,
	sts_match(TB, TR, BC, BC, NB).
sts_match([[X, Y, _] | _], [[X, Y] | _], _, _, _) :- !, fail.

sts_match([[X, Y, C] | TB], [[_, RY] | TL], CCLR, CLR, NB) :- Y > RY, !,
	sts_match([[X, Y, C] | TB], TL, CCLR, CLR, NB).
sts_match([[X, Y, C] | TB], [[RX, Y] | TL], CCLR, CLR, NB) :- X > RX, !,
	sts_match([[X, Y, C] | TB], TL, CCLR, CLR, NB).

sts_match([C | TB], R, CCLR, CLR, [C | TNB]) :-
	sts_match(TB, R, CCLR, CLR, TNB).

%%%%%%%
%
% The testing scaffold: solve a trivial board and a not-so-trivial one.
%
% NB: this uses the YAP-specific directive to output lists that look like
% ASCII codes as strings.  This may be removed or unneeded for other Prolog
% implementations.

c(B) :- write('Solving for '), write(B), write(':'), nl, strata_solver(B, S),
	yap_flag(write_strings, on), write(S), nl.

:- nl, c([[1, 1], [2, 0]]), nl, c([[1, 2, 1], [1, 3, 3], [1, 4, 1]]), nl.
